#ifndef PACKETSTRUCTURE_H
#define PACKETSTRUCTURE_H
#include <stdio.h>

#include <inttypes.h>

#pragma pack(push,1)
struct longStructure {
    uint8_t day;
    uint8_t month;
    uint16_t year;
    uint8_t min;
    uint8_t hour;
    uint8_t sec;
    float data[100];
};

struct shortStructure {
    uint8_t day;
    uint8_t month;
    uint16_t year;
    uint8_t min;
    uint8_t hour;
    uint8_t sec;
    float data[10];
};


#pragma pack(pop)


#endif // PACKETSTRUCTURE_H
