#include <QCoreApplication>
#include "myclient.h"
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    MyClient client("192.168.113.37", 2323);
    QTimer timer;
    QObject::connect(&timer,SIGNAL(timeout()),
            &client,SLOT(slotSendToServer()));
    timer.setTimerType(Qt::CoarseTimer);
    timer.start(1000);

    return a.exec();
}
