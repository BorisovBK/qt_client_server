#include "myclient.h"


#include <QAbstractSocket>
#include <QDataStream>
#include <QTime>

MyClient::MyClient(const QString &strHost,
                   int nPort): m_nNextBlockSize(0),timer()
{
    m_pTcpSocket = new QTcpSocket(this);
    m_pTcpSocket->connectToHost(strHost, nPort);
    connect(m_pTcpSocket, SIGNAL(connected()), SLOT(slotConnected()));
    connect(m_pTcpSocket, SIGNAL(readyRead()), SLOT(slotReadyRead()));
    connect(m_pTcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(slotError(QAbstractSocket::SocketError)));

    //    m_ptxtInfo = new QTextEdit;
    //    m_ptxtInput = new QLineEdit;

    //    m_ptxtInfo->setReadOnly(true);

    //    QPushButton *pcmd = new QPushButton("&Send");
    //    connect(pcmd, SIGNAL(clicked()), SLOT(slotSendToServer()));
    //    connect(m_ptxtInput, SIGNAL(returnPressed()),
    //            this, SLOT(slotSendToServer())
    //            );

    //layout setup
    //    QVBoxLayout *lay = new QVBoxLayout;
    //    lay->addWidget(new QLabel("<h1>Client</h1>"));
    //    lay->addWidget(m_ptxtInfo);
    //    lay->addWidget(m_ptxtInput);
    //    lay->addWidget(pcmd);
    //    setLayout(lay);
}

void MyClient::slotReadyRead()
{
    QDataStream in(m_pTcpSocket);
    in.setVersion(QDataStream::Qt_5_11);
    for (;;) {
        if (!m_nNextBlockSize){
            if (m_pTcpSocket->bytesAvailable() < sizeof(quint16)){
                break;
            }
            in >> m_nNextBlockSize;
        }
        if (m_pTcpSocket->bytesAvailable() < m_nNextBlockSize)
        {
            break;
        }
        //        QTime  time;
        QString str;
        in >> str;

        qDebug() << str;
        m_nNextBlockSize = 0;
    }

}

void MyClient::slotError(QAbstractSocket::SocketError err){
    QString strError =
            "Error: " + (err == QAbstractSocket::HostNotFoundError ?
                             "The host was not found." :
                             err == QAbstractSocket::RemoteHostClosedError ?
                                 "The remote host is closed." :
                                 err == QAbstractSocket::ConnectionRefusedError ?
                                     "The connection was redused." :
                                     QString(m_pTcpSocket->errorString())
                                     );
    qDebug() << strError;
}

void MyClient::slotSendToServer()
{
    QByteArray arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_11);
    QString str("World!");
    out << quint16(0) << str;
    out.device()->seek(0);
    out << quint16(arrBlock.size() - sizeof(quint16));

    m_pTcpSocket->write(arrBlock);
//    m_ptxtInput->setText("");
}

void MyClient::slotSendToServer(QByteArray data)
{
    QByteArray arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_11);

    out << quint16(0) << data;
    out.device()->seek(0);
    out << quint16(arrBlock.size() - sizeof(quint16));

    m_pTcpSocket->write(arrBlock);
//    m_ptxtInput->setText("");
}

void MyClient::slotConnected()
{
    qDebug() << "Received the connected() signal";
}
