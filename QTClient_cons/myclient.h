#ifndef MYCLIENT_H
#define MYCLIENT_H


#include <QTcpSocket>
#include <QObject>
#include <QAbstractSocket>
#include <QDataStream>
#include <QTime>
#include <QTimer>

class QTextEdit;
class QLineEdit;

class MyClient : public QObject
{
    Q_OBJECT

private:
    QTcpSocket *m_pTcpSocket;
//    QTextEdit *m_ptxtInfo;
//    QLineEdit *m_ptxtInput;
    quint16 m_nNextBlockSize;
    QTimer timer;

public:
    MyClient(const QString &strHost, int nPort);

private slots:
    void slotReadyRead();
    void slotError(QAbstractSocket::SocketError);
    void slotSendToServer(QByteArray data);
    void slotSendToServer();
    void slotConnected();

};

#endif // MYCLIENT_H
