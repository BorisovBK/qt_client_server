#pragma once

#include <QObject>
//#include <QWidget>
#include <QTcpServer>
#include <QTcpSocket>
#include <QString>

class QTcpServer;
class QTextEdit;
class QTcpSocket;
class QObject;

class MyServer : QObject
{
    Q_OBJECT

private:
    QTcpServer* m_ptcpServer;

    quint16 m_nNextBlockSize;
    void sendToClient(QTcpSocket *pSocket, const QString &str);

public:
    MyServer(int nPort);

public slots:
    virtual void slotNewConnection();
            void slotReadClient();
};

