#include "myserver.h"


#include <QObject>
//#include <QTcpServer>
//#include <QHostAddress>
#include <QDataStream>


MyServer::MyServer(int nPort)
    : m_nNextBlockSize(0)
{
    m_ptcpServer = new QTcpServer(this);
    if (!m_ptcpServer->listen(QHostAddress::Any, nPort)){
        //        QMessageBox::critical(0,
        //                              "Server error",
        //                              "Unable to start the server:"
        //                              + m_ptcpServer->errorString()
        //                              );
        qDebug() << "Error listenning !\n";
        m_ptcpServer->close();
        return;
    }
    connect(m_ptcpServer, SIGNAL(newConnection()),
            this,         SLOT(slotNewConnection())
            );
    qDebug() << "Start listenning \n";
    //    m_ptxt = new QTextEdit;
    //    m_ptxt->setReadOnly(true);

    //Layout setup
    //    QVBoxLayout* lay = new QVBoxLayout;
    //    lay->addWidget(new QLabel("<h1>Server</h1>"));
    //    lay->addWidget(m_ptxt);
    //    setLayout(lay);
}

/*virtual*/
void MyServer::slotNewConnection()
{
    QTcpSocket* pClientSocket = m_ptcpServer->nextPendingConnection();
    connect(pClientSocket, SIGNAL(disconnected()),
            pClientSocket, SLOT(deleteLater())
            );
    connect(pClientSocket, SIGNAL(readyRead()),
            this,          SLOT(slotReadClient())
            );
    sendToClient(pClientSocket ,QString("Server Responce: Connected!"));
}

void MyServer::slotReadClient()
{
    QTcpSocket* pClientSocket = (QTcpSocket*)sender();
    QDataStream in(pClientSocket);
    in.setVersion(QDataStream::Qt_5_11);
    for (;;)
    {
        if(!m_nNextBlockSize){
            if (pClientSocket->bytesAvailable() < sizeof(quint16)){
                break;
            }
            in >> m_nNextBlockSize;
        }
        if (pClientSocket->bytesAvailable() < m_nNextBlockSize){
            break;
        }
        //        QTime   time;
        QString str;
        in >> str;
        qDebug() << "Client message:" + str;
        //        QString strMessage = time.toString() + "" + "Client has sent - " + str;
        //        m_ptxt->append(strMessage);

        m_nNextBlockSize = 0;
        sendToClient(pClientSocket,
                     "Server Responce: Received \"" + str + "\"");
    }
}

void MyServer::sendToClient(QTcpSocket* pSocket, const QString& str)
{
    QByteArray  arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_11);

    out << quint16(0) << str;

    out.device()->seek(0);
    out << quint16(arrBlock.size() - sizeof(quint16));

    pSocket->write(arrBlock);
}
