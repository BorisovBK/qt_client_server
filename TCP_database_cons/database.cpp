#include "database.h"

DataBase::DataBase(QObject *parent) : QObject(parent),strList()
{
    createListOfVar();
}

DataBase::~DataBase()
{

}

/* Методы для подключения к базе данных
 * */
void DataBase::connectToDataBase()
{
    /* Перед подключением к базе данных производим проверку на её существование.
     * В зависимости от результата производим открытие базы данных или её восстановление
     * */
    //    if(!QFile("C:/example/" DATABASE_NAME).exists()){
    if(!QFile(DATABASE_NAME).exists()){
        this->restoreDataBase();
    } else {
        this->openDataBase();
    }
}

/* Методы восстановления базы данных
 * */
bool DataBase::restoreDataBase()
{
    if(this->openDataBase()){
        if(!this->createTable()){
            return false;
        } else {
            return true;
        }
    } else {
        qDebug() << "Не удалось восстановить базу данных";
        return false;
    }
    return false;
}

/* Метод для открытия базы данных
 * */
bool DataBase::openDataBase()
{
    /* База данных открывается по заданному пути
     * и имени базы данных, если она существует
     * */
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setHostName(DATABASE_HOSTNAME);
    db.setDatabaseName(DATABASE_NAME);
    if(db.open()){
        return true;
    } else {
        return false;
    }
}

/* Методы закрытия базы данных
 * */
void DataBase::closeDataBase()
{
    db.close();
}

/* Метод для создания таблицы в базе данных
 * */
bool DataBase::createTable()
{
    /* В данном случае используется формирование сырого SQL-запроса
     * с последующим его выполнением.
     * */

    char stringHeaderToCreate[1000];


    sprintf(stringHeaderToCreate,"%s","CREATE TABLE " TABLE " ("
                                                            "id INTEGER PRIMARY KEY AUTOINCREMENT, "
            TABLE_DATE      " DATE            NOT NULL,"
            TABLE_TIME      " TIME            NOT NULL,"
            );
    QString startOfQuerry(stringHeaderToCreate);
    QString bodyOfQuerry("");

    for (int ii = 0;ii<strList.size()-1;ii++)
    {
        bodyOfQuerry = bodyOfQuerry+QString(strList[ii])+  QString(" INTEGER NOT NULL")  +   QString(",");
    }
    bodyOfQuerry = bodyOfQuerry+strList[strList.size()-1] + QString(" INTEGER NOT NULL")  +   QString(")");

    //    memset(stringToconcat,0,100);
    //    sprintf(stringToconcat,"vN%02d INTEGER NOT NULL )",4);
    //    strcat(stringMyasso,stringToconcat);

    //    strcat(stringToCreate,stringHeaderToCreate);
    //    strcat(stringToCreate,stringMyasso);


    //    printf("%s\n","CREATE TABLE " TABLE " ("
    //                                        "id INTEGER PRIMARY KEY AUTOINCREMENT, "
    //           TABLE_DATE      " DATE            NOT NULL,"
    //           TABLE_TIME      " TIME            NOT NULL,"
    //           TABLE_RANDOM    " INTEGER         NOT NULL,"
    //           TABLE_MESSAGE   " VARCHAR(255)    NOT NULL"
    //                           " )");

    //    printf("%s\n",stringToCreate);
    //    QSqlQuery query;
    //    if(!query.exec( "CREATE TABLE " TABLE " ("
    //                    "id INTEGER PRIMARY KEY AUTOINCREMENT, "
    //                    TABLE_DATE      " DATE            NOT NULL,"
    //                    TABLE_TIME      " TIME            NOT NULL,"
    //                    TABLE_RANDOM    " INTEGER         NOT NULL,"
    //                    TABLE_MESSAGE   " VARCHAR(255)    NOT NULL"
    //                    " )"
    //                    )){
    //        qDebug() << "DataBase: error of create " << TABLE;
    //        qDebug() << query.lastError().text();
    //        return false;
    //    } else {
    //        return true;
    //    }

    QSqlQuery query;
    //    if(!query.exec(stringToCreate)){
    if(!query.exec(startOfQuerry+bodyOfQuerry)){
        qDebug() << "DataBase: error of create " << TABLE;
        qDebug() << query.lastError().text();
        return false;
    } else {
        return true;
    }
    return false;
}

/* Метод для вставки записи в базу данных
 * */
bool DataBase::inserIntoTable(const QVariantList &data)
{
    /* Запрос SQL формируется из QVariantList,
     * в который передаются данные для вставки в таблицу.
     * */
    QSqlQuery query;
    /* В начале SQL запрос формируется с ключами,
     * которые потом связываются методом bindValue
     * для подстановки данных из QVariantList
     * */
    //    query.prepare("INSERT INTO " TABLE " ( " TABLE_DATE ", "
    //                  TABLE_TIME ", "
    //                  TABLE_RANDOM ", "
    //                  TABLE_MESSAGE " ) "
    //                                "VALUES (:Date, :Time, :Random, :Message )");
    QString startOfPrepare("INSERT INTO " TABLE " ( " TABLE_DATE ", "
                           TABLE_TIME ", ");

    for (int i = 0;i<strList.size()-1;i++)
    {
        startOfPrepare = startOfPrepare + strList[i] + QString(",");
    }
    startOfPrepare = startOfPrepare + strList[strList.size()-1] + QString(") ");


    startOfPrepare = startOfPrepare + QString("VALUES (:Date, :Time, ");
    for (int i = 0;i<strList.size()-1;i++)
    {
        startOfPrepare = startOfPrepare + QString(":") + strList[i] + QString(", ");
    }
    startOfPrepare = startOfPrepare + QString(":") + strList[strList.size()-1] + QString(")");

    qDebug() << query.prepare(startOfPrepare);
    qDebug() << startOfPrepare;

    query.bindValue(":Date",        data[0].toDate());
    query.bindValue(":Time",        data[1].toTime());

    for (int i = 0; i<strList.size(); i++)
    {
        query.bindValue(QString(":") + strList[i],      data[i+2].toInt());
    }

    // После чего выполняется запросом методом exec()
    if(!query.exec()){
        qDebug() << "error insert into " << TABLE;
        qDebug() << query.lastError().text();
        return false;
    } else {
        return true;
    }
    return false;
}

void DataBase::createListOfVar()
{
    char strOfName[200];
    for (int ii = 0;ii<4;ii++)
    {
        memset(strOfName,0,100);
        sprintf(strOfName,"vN%02d",ii+1);
        strList.append(QString(strOfName));
    }
    qDebug() << strList;
}
